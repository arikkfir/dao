package org.arikkfir.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.arikkfir.dao.*;
import org.arikkfir.dao.support.AbstractTests;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

/**
 * @author arik
 * @on 1/13/15.
 */
public class DaoTests extends AbstractTests
{
    public interface Dao1
    {
    }

    public interface Dao2
    {
    }

    public interface DaoWithIllegalMethods
    {
        void m1();

        Object m2();
    }

    public interface QueriesDao
    {
        @Query( "SELECT COUNT(*) FROM t1 " )
        int errorRowSetParserANDRowParser( @Nonnull ResultSetExtractor<Object> rowSetParser,
                                           @Nonnull RowMapper<Object> rowParser );

        @Query( "SELECT COUNT(*) FROM t1 " )
        int countAll();

        @Query( "SELECT COUNT(*) FROM t1 " )
        int wrongReturnTypeAndRowSetParserType( @Nonnull ResultSetExtractor<String> rowSetParser );

        @Query( "SELECT COUNT(*) FROM t1 " )
        String countAllWithRowSetParser( @Nonnull ResultSetExtractor<String> rowSetParser );

        @Query( "SELECT COUNT(*) FROM t1 WHERE s LIKE :pattern " )
        int countByPattern( @Nonnull String pattern );

        @Query( "SELECT s FROM t1 " )
        List<String> selectStrings();

        @Query( "SELECT s FROM t1 WHERE s = 'Unknown' " )
        @Nonnull
        String selectOneAndTransformRequiredString( @Nonnull RowMapper<String> rowParser );

        @Query( "SELECT s FROM t1 WHERE s = 'Unknown' " )
        @Nullable
        String selectOneAndTransformOptionalString( @Nonnull RowMapper<String> rowParser );

        @Query( "SELECT s FROM t1 " )
        List<Integer> wrongReturnTypeListItemAndRowParserItem( @Nonnull RowMapper<String> rowParser );

        @Query( "SELECT s FROM t1 " )
        List<String> selectAndTransformStrings( @Nonnull RowMapper<String> rowParser );

        @Query( "SELECT s FROM t1 WHERE s LIKE :pattern " )
        List<String> selectAndTransformStringsByPattern( @Nonnull String pattern,
                                                         @Nonnull RowMapper<String> rowParser );

        @Query( value = "SELECT s FROM t1 WHERE s LIKE :pattern ", rowMapper = StringRowMapper.class )
        List<String> selectAndTransformStringsByPatternWithBuiltinRowMapper( @Nonnull String pattern );

        @Query( value = "SELECT s FROM t1 WHERE s LIKE :pattern ", resultSetExtractor = StringResultSetExtractor.class )
        List<String> selectAndTransformStringsByPatternWithBuiltinResultSetExtractor( @Nonnull String pattern );
    }

    public interface UpdatesDao
    {
        @Update( "UPDATE t1 SET s = 'Again' WHERE s = :value " )
        int update( @Nonnull String value );

        @Update( "UPDATE t1 SET s = 'Again' WHERE s = :value " )
        String errorUpdateMustReturnInt( @Nonnull String value );
    }

    @Table( "t1" )
    public interface T1
    {
        @Id
        @Column( "id" )
        int getId();

        @Column( "s" )
        String getName();
    }

    public interface TableDao
    {
        @Nonnull
        T1 create( @Nonnull String name );

        @Nullable
        @Query( "SELECT s FROM t1 WHERE s = :value " )
        String optionalFindValue( @Nonnull String value );

        @Nullable
        @Query( "SELECT id FROM t1 WHERE s = :value " )
        T1 optionalFindEntity( @Nonnull String value );
    }

    public static class StringRowMapper implements RowMapper<String>
    {
        @Override
        public String mapRow( ResultSet rs, int rowNum ) throws SQLException
        {
            return rs.getString( 1 );
        }
    }

    public static class StringResultSetExtractor implements ResultSetExtractor<List<String>>
    {
        @Override
        public List<String> extractData( ResultSet rs ) throws SQLException, DataAccessException
        {
            List<String> strings = new LinkedList<>();
            while( rs.next() )
            {
                strings.add( rs.getString( 1 ) );
            }
            return strings;
        }
    }

    private DaoFactoryImpl daoFactory;

    @Override
    @Before
    public void setUp() throws Exception
    {
        super.setUp();
        this.daoFactory = new DaoFactoryImpl( this.jdbcTemplate, this.namedParameterJdbcTemplate );
    }

    @Override
    @After
    public void tearDown() throws Exception
    {
        this.daoFactory.destroy();
        this.daoFactory = null;
        super.tearDown();
    }

    @Test
    public void shouldEqualOnSameTypeOnly()
    {
        Dao1 firstDao1 = new DaoFactoryImpl( this.jdbcTemplate, this.namedParameterJdbcTemplate ).create( Dao1.class );
        Dao1 secondDao1 = new DaoFactoryImpl( this.jdbcTemplate, this.namedParameterJdbcTemplate ).create( Dao1.class );
        Dao2 dao2 = new DaoFactoryImpl( this.jdbcTemplate, this.namedParameterJdbcTemplate ).create( Dao2.class );
        Dao1 customDao1 = new Dao1()
        {
        };

        assertEquals( firstDao1, secondDao1 );
        assertNotEquals( firstDao1, dao2 );
        assertNotEquals( secondDao1, dao2 );
        assertNotEquals( firstDao1, customDao1 );
        assertNotEquals( secondDao1, customDao1 );
    }

    @Test( expected = IllegalStateException.class )
    public void errorRowSetParserANDRowParser()
    {
        this.daoFactory.create( QueriesDao.class ).errorRowSetParserANDRowParser( resultSet -> resultSet, ( resultSet1,
                                                                                                            rowNum ) -> rowNum );
    }

    @Test
    public void countAll()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        assertEquals( 4, this.daoFactory.create( QueriesDao.class ).countAll() );
    }

    @Test( expected = IllegalStateException.class )
    public void wrongReturnTypeAndRowSetParserType()
    {
        assertEquals( 4, this.daoFactory.create( QueriesDao.class ).wrongReturnTypeAndRowSetParserType( resultSet -> "" ) );
    }

    @Test
    public void countAllWithRowSetParser()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        assertEquals( "test", this.daoFactory.create( QueriesDao.class ).countAllWithRowSetParser( resultSet -> "test" ) );
    }

    @Test
    public void countByPattern()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        assertEquals( 2, this.daoFactory.create( QueriesDao.class ).countByPattern( "%a%" ) );
    }

    @Test
    public void selectStrings()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        assertEquals( asList( "Joe", "Black", "Again", null ), this.daoFactory.create( QueriesDao.class ).selectStrings() );
    }

    @Test
    public void selectAndTransformStrings()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        QueriesDao dao = this.daoFactory.create( QueriesDao.class );
        assertEquals( asList( "Joe!", "Black!", "Again!", "null!" ), dao.selectAndTransformStrings( ( resultSet,
                                                                                                      rowNum ) -> resultSet.getString( 1 ) + "!" ) );
    }

    @Test( expected = IncorrectResultSizeDataAccessException.class )
    public void selectOneAndTransformRequiredString()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        QueriesDao dao = this.daoFactory.create( QueriesDao.class );
        dao.selectOneAndTransformRequiredString( ( resultSet, rowNum ) -> resultSet.getString( 1 ) + "!" );
    }

    @Test( expected = IllegalStateException.class )
    public void wrongReturnTypeListItemAndRowParserItem()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        QueriesDao dao = this.daoFactory.create( QueriesDao.class );
        dao.wrongReturnTypeListItemAndRowParserItem( ( resultSet, rowNum ) -> resultSet.getString( 1 ) + "!" );
    }

    @Test
    public void selectOneAndTransformOptionalString()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        QueriesDao dao = this.daoFactory.create( QueriesDao.class );
        assertNull( dao.selectOneAndTransformOptionalString( ( resultSet,
                                                               rowNum ) -> resultSet.getString( 1 ) + "!" ) );
    }

    @Test
    public void selectAndTransformStringsByPattern()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        QueriesDao dao = this.daoFactory.create( QueriesDao.class );
        assertEquals( asList( "Black!", "Again!" ),
                      dao.selectAndTransformStringsByPattern( "%a%", ( resultSet,
                                                                       rowNum ) -> resultSet.getString( 1 ) + "!" ) );
    }

    @Test
    public void selectAndTransformStringsByPatternWithBuiltinRowMapper()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        QueriesDao dao = this.daoFactory.create( QueriesDao.class );
        assertEquals( asList( "Black", "Again" ), dao.selectAndTransformStringsByPatternWithBuiltinRowMapper( "%a%" ) );
    }

    @Test
    public void selectAndTransformStringsByPatternWithBuiltinResultSetExtractor()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), ('Again'), (NULL)" );
        QueriesDao dao = this.daoFactory.create( QueriesDao.class );
        assertEquals( asList( "Black", "Again" ), dao.selectAndTransformStringsByPatternWithBuiltinResultSetExtractor( "%a%" ) );
    }

    @Test
    public void updateDao()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), (NULL)" );
        UpdatesDao dao = this.daoFactory.create( UpdatesDao.class );
        assertEquals( 1, dao.update( "Black" ) );
        assertEquals( asList( "Joe", null, "Again" ), this.daoFactory.create( QueriesDao.class ).selectStrings() );
    }

    @Test( expected = IllegalStateException.class )
    public void errorUpdateMustReturnInt()
    {
        UpdatesDao dao = this.daoFactory.create( UpdatesDao.class );
        dao.errorUpdateMustReturnInt( "Black" );
    }

    @Test( expected = UnsupportedOperationException.class )
    public void illegalMethod1()
    {
        DaoWithIllegalMethods dao = this.daoFactory.create( DaoWithIllegalMethods.class );
        dao.m1();
    }

    @Test( expected = UnsupportedOperationException.class )
    public void illegalMethod2()
    {
        DaoWithIllegalMethods dao = this.daoFactory.create( DaoWithIllegalMethods.class );
        dao.m2();
    }

    @Test
    public void findByName()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( id INTEGER GENERATED BY DEFAULT AS IDENTITY, s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), (NULL)" );
        TableDao dao = this.daoFactory.create( TableDao.class );
        T1 again = dao.create( "Again" );
        assertEquals( "Again", again.getName() );
        assertEquals( 3, again.getId() );
        assertEquals( asList( "Joe", "Black", null, "Again" ), this.daoFactory.create( QueriesDao.class ).selectStrings() );
    }

    @Test
    public void nullReturnedOnNullablefinderWithNoResults()
    {
        this.jdbcTemplate.update( "CREATE TABLE t1 ( id INTEGER GENERATED BY DEFAULT AS IDENTITY, s VARCHAR(255) )" );
        TableDao dao = this.daoFactory.create( TableDao.class );
        assertNull( dao.optionalFindValue( "nonExisting" ) );
        assertNull( dao.optionalFindEntity( "nonExisting" ) );

        this.jdbcTemplate.update( "INSERT INTO t1 (s) VALUES ('Joe'), ('Black'), (NULL)" );
        assertEquals( "Joe", dao.optionalFindValue( "Joe" ) );
        assertEquals( "Joe", dao.optionalFindEntity( "Joe" ).getName() );
    }
}
