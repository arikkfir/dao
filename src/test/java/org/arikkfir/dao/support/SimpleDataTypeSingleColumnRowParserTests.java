package org.arikkfir.dao.support;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.sql.Time;
import java.time.*;
import java.util.Arrays;
import java.util.Locale;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.junit.Test;
import org.springframework.jdbc.IncorrectResultSetColumnCountException;
import org.springframework.jdbc.core.RowMapper;

import static org.junit.Assert.*;

/**
 * @author arik
 * @on 1/4/15.
 */
public class SimpleDataTypeSingleColumnRowParserTests extends AbstractTests
{
    public static enum TestEnum
    {
        JOE, BLACK
    }

    @Test( expected = IncorrectResultSetColumnCountException.class )
    public void enforcesSingleColumn() throws SQLException
    {
        this.jdbcTemplate.update( "CREATE TABLE t ( c1 INT, c2 INT )" );
        this.jdbcTemplate.update( "INSERT INTO t (c1, c2) VALUES (1, 2)" );
        ass( "SELECT c1, c2 FROM t ", Integer.class, 1024 );
    }

    @Test
    public void numerics() throws SQLException
    {
        //
        // BigDecimal
        //
        this.jdbcTemplate.update( "CREATE TABLE t1 ( n DECIMAL(8,4) )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (n) VALUES (1024.2048),(NULL)" );
        ass( "SELECT n FROM t1 WHERE n IS NOT NULL ", BigDecimal.class, new BigDecimal( "1024.2048" ) );
        ass( "SELECT n FROM t1 WHERE n IS NULL ", BigDecimal.class, null );

        //
        // BigInteger
        //
        this.jdbcTemplate.update( "CREATE TABLE t2 ( n DECIMAL(20) )" );
        this.jdbcTemplate.update( "INSERT INTO t2 (n) VALUES (12345678901234567890),(NULL)" );
        ass( "SELECT n FROM t2 WHERE n IS NOT NULL ", BigInteger.class, new BigInteger( "12345678901234567890" ) );
        ass( "SELECT n FROM t2 WHERE n IS NULL ", BigInteger.class, null );

        //
        // byte
        //
        this.jdbcTemplate.update( "CREATE TABLE t3 ( b TINYINT )" );
        this.jdbcTemplate.update( "INSERT INTO t3 (b) VALUES (100), (NULL)" );
        ass( "SELECT b FROM t3 WHERE b = 100 ", byte.class, ( byte ) 100 );
        ass( "SELECT b FROM t3 WHERE b IS NULL ", byte.class, ( byte ) 0 );
        ass( "SELECT b FROM t3 WHERE b = 100 ", Byte.class, new Byte( "100" ) );
        ass( "SELECT b FROM t3 WHERE b IS NULL ", Byte.class, null );

        //
        // double
        //
        this.jdbcTemplate.update( "CREATE TABLE t4 ( d DOUBLE )" );
        this.jdbcTemplate.update( "INSERT INTO t4 (d) VALUES (32767.456), (-32768.123), (NULL)" );
        ass( "SELECT d FROM t4 WHERE d < 0 ", double.class, -32768.123 );
        ass( "SELECT d FROM t4 WHERE d > 0 ", double.class, 32767.456 );
        ass( "SELECT d FROM t4 WHERE d IS NULL ", double.class, ( double ) 0 );
        ass( "SELECT d FROM t4 WHERE d < 0 ", Double.class, new Double( "-32768.123" ) );
        ass( "SELECT d FROM t4 WHERE d > 0 ", Double.class, new Double( "32767.456" ) );
        ass( "SELECT d FROM t4 WHERE d IS NULL ", Double.class, null );

        //
        // float
        //
        this.jdbcTemplate.update( "CREATE TABLE t5 ( f FLOAT )" );
        this.jdbcTemplate.update( "INSERT INTO t5 (f) VALUES (32767.456), (-32768.123), (NULL)" );
        ass( "SELECT f FROM t5 WHERE f < 0 ", float.class, ( float ) -32768.123 );
        ass( "SELECT f FROM t5 WHERE f > 0 ", float.class, ( float ) 32767.456 );
        ass( "SELECT f FROM t5 WHERE f IS NULL ", float.class, ( float ) 0 );
        ass( "SELECT f FROM t5 WHERE f < 0 ", Float.class, new Float( "-32768.123" ) );
        ass( "SELECT f FROM t5 WHERE f > 0 ", Float.class, new Float( "32767.456" ) );
        ass( "SELECT f FROM t5 WHERE f IS NULL ", Float.class, null );

        //
        // integer
        //
        this.jdbcTemplate.update( "CREATE TABLE t6 ( i INTEGER )" );
        this.jdbcTemplate.update( "INSERT INTO t6 (i) VALUES (32767.456), (-32768.123), (NULL)" );
        ass( "SELECT i FROM t6 WHERE i < 0 ", int.class, -32768 );
        ass( "SELECT i FROM t6 WHERE i > 0 ", int.class, 32767 );
        ass( "SELECT i FROM t6 WHERE i IS NULL ", int.class, 0 );
        ass( "SELECT i FROM t6 WHERE i < 0 ", Integer.class, new Integer( "-32768" ) );
        ass( "SELECT i FROM t6 WHERE i > 0 ", Integer.class, new Integer( "32767" ) );
        ass( "SELECT i FROM t6 WHERE i IS NULL ", Integer.class, null );

        //
        // long
        //
        this.jdbcTemplate.update( "CREATE TABLE t7 ( l INTEGER )" );
        this.jdbcTemplate.update( "INSERT INTO t7 (l) VALUES (-290000000), (280000000), (NULL)" );
        ass( "SELECT l FROM t7 WHERE l < 0 ", long.class, ( long ) -290000000 );
        ass( "SELECT l FROM t7 WHERE l > 0 ", long.class, ( long ) 280000000 );
        ass( "SELECT l FROM t7 WHERE l IS NULL ", long.class, ( long ) 0 );
        ass( "SELECT l FROM t7 WHERE l < 0 ", Long.class, new Long( "-290000000" ) );
        ass( "SELECT l FROM t7 WHERE l > 0 ", Long.class, new Long( "280000000" ) );
        ass( "SELECT l FROM t7 WHERE l IS NULL ", Long.class, null );

        //
        // short
        //
        this.jdbcTemplate.update( "CREATE TABLE t8 ( s SMALLINT )" );
        this.jdbcTemplate.update( "INSERT INTO t8 (s) VALUES (-2048), (2047), (NULL)" );
        ass( "SELECT s FROM t8 WHERE s < 0 ", short.class, ( short ) -2048 );
        ass( "SELECT s FROM t8 WHERE s > 0 ", short.class, ( short ) 2047 );
        ass( "SELECT s FROM t8 WHERE s IS NULL ", short.class, ( short ) 0 );
        ass( "SELECT s FROM t8 WHERE s < 0 ", Short.class, new Short( "-2048" ) );
        ass( "SELECT s FROM t8 WHERE s > 0 ", Short.class, new Short( "2047" ) );
        ass( "SELECT s FROM t8 WHERE s IS NULL ", Short.class, null );
    }

    @Test
    public void booleans() throws SQLException
    {
        this.jdbcTemplate.update( "CREATE TABLE t ( b BOOLEAN )" );
        this.jdbcTemplate.update( "INSERT INTO t (b) VALUES (TRUE),(FALSE),(NULL)" );
        ass( "SELECT b FROM t WHERE b = TRUE ", boolean.class, true );
        ass( "SELECT b FROM t WHERE b = FALSE ", boolean.class, false );
        ass( "SELECT b FROM t WHERE b IS NULL ", boolean.class, false );
        ass( "SELECT b FROM t WHERE b = TRUE ", Boolean.class, true );
        ass( "SELECT b FROM t WHERE b = FALSE ", Boolean.class, false );
        ass( "SELECT b FROM t WHERE b IS NULL ", Boolean.class, null );
    }

    @Test
    public void blob() throws SQLException
    {
        this.jdbcTemplate.update( "CREATE TABLE t ( b VARBINARY(10) )" );

        byte[] bytes = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        this.jdbcTemplate.update( "INSERT INTO t (b) VALUES (?)", ( Object ) bytes );
        byte[] fetchedBytes = this.jdbcTemplate.queryForObject( "SELECT b FROM t ", new SimpleDataTypeSingleColumnRowParser<>( byte[].class ) );
        assertTrue( Arrays.equals( bytes, fetchedBytes ) );

        this.jdbcTemplate.update( "UPDATE t SET b = NULL" );
        assertNull( this.jdbcTemplate.queryForObject( "SELECT b FROM t ", new SimpleDataTypeSingleColumnRowParser<>( byte[].class ) ) );
    }

    @Test
    public void chars() throws SQLException
    {
        this.jdbcTemplate.update( "CREATE TABLE t ( s VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t (s) VALUES ('Joe'), ('Black'), (''), (NULL)" );
        ass( "SELECT s FROM t WHERE s = 'Joe' ", char.class, 'J' );
        ass( "SELECT s FROM t WHERE s = 'Black' ", char.class, 'B' );
        ass( "SELECT s FROM t WHERE s = '' ", char.class, ( char ) 0 );
        ass( "SELECT s FROM t WHERE s IS NULL ", char.class, ( char ) 0 );
        ass( "SELECT s FROM t WHERE s = 'Joe' ", Character.class, 'J' );
        ass( "SELECT s FROM t WHERE s = 'Black' ", Character.class, 'B' );
        ass( "SELECT s FROM t WHERE s = '' ", Character.class, null );
        ass( "SELECT s FROM t WHERE s IS NULL ", Character.class, null );
        ass( "SELECT s FROM t WHERE s = 'Joe' ", String.class, "Joe" );
        ass( "SELECT s FROM t WHERE s IS NULL ", String.class, null );
    }

    @Test
    public void dates() throws SQLException
    {
        ZoneId systemZone = ZoneId.systemDefault();

        //
        // java.sql.Date
        //
        this.jdbcTemplate.update( "CREATE TABLE t1 ( d DATE )" );
        this.jdbcTemplate.update( "INSERT INTO t1 (d) VALUES ('2014-12-31'), (NULL)" );
        java.sql.Date d1 = this.jdbcTemplate.queryForObject( "SELECT d FROM t1 WHERE d = '2014-12-31'",
                                                             new SimpleDataTypeSingleColumnRowParser<>( java.sql.Date.class ) );
        assertEquals( d1.toLocalDate(), LocalDate.of( 2014, 12, 31 ) );
        assertNull( this.jdbcTemplate.queryForObject( "SELECT d FROM t1 WHERE d IS NULL ", new SimpleDataTypeSingleColumnRowParser<>( java.sql.Date.class ) ) );

        //
        // Instant
        //
        this.jdbcTemplate.update( "CREATE TABLE t2 ( tsntz TIMESTAMP WITHOUT TIME ZONE, d DATE )" );
        this.jdbcTemplate.update( "INSERT INTO t2 (tsntz,d) VALUES ('2014-12-31 23:00:00', '2014-12-31'), (NULL, NULL)" );
        ass( "SELECT tsntz FROM t2 WHERE tsntz IS NOT NULL ", Instant.class, ZonedDateTime.of( 2014, 12, 31, 23, 0, 0, 0, systemZone ).toInstant() );
        ass( "SELECT tsntz FROM t2 WHERE tsntz IS NULL ", Instant.class, null );
        ass( "SELECT d FROM t2 WHERE d IS NOT NULL ", Instant.class, ZonedDateTime.of( 2014, 12, 31, 0, 0, 0, 0, systemZone ).toInstant() );
        ass( "SELECT d FROM t2 WHERE d IS NULL ", Instant.class, null );

        //
        // LocalDate
        //
        ass( "SELECT tsntz FROM t2 WHERE tsntz IS NOT NULL ", LocalDate.class, LocalDate.of( 2014, 12, 31 ) );
        ass( "SELECT tsntz FROM t2 WHERE tsntz IS NULL ", LocalDate.class, null );
        ass( "SELECT d FROM t2 WHERE d IS NOT NULL ", LocalDate.class, LocalDate.of( 2014, 12, 31 ) );
        ass( "SELECT d FROM t2 WHERE d IS NULL ", LocalDate.class, null );

        //
        // LocalDateTime
        //
        ass( "SELECT tsntz FROM t2 WHERE tsntz IS NOT NULL ", LocalDateTime.class, LocalDateTime.of( 2014, 12, 31, 23, 0, 0 ) );
        ass( "SELECT tsntz FROM t2 WHERE tsntz IS NULL ", LocalDateTime.class, null );
        ass( "SELECT d FROM t2 WHERE d IS NOT NULL ", LocalDateTime.class, LocalDateTime.of( 2014, 12, 31, 0, 0, 0 ) );
        ass( "SELECT d FROM t2 WHERE d IS NULL ", LocalDateTime.class, null );

        //
        // LocalTime
        //
        this.jdbcTemplate.update( "CREATE TABLE t3 ( tsntz TIMESTAMP WITHOUT TIME ZONE, t TIME )" );
        this.jdbcTemplate.update( "INSERT INTO t3 (tsntz,t) VALUES ('2014-12-31 23:00:00', '09:50:00'), (NULL, NULL)" );
        ass( "SELECT tsntz FROM t3 WHERE tsntz IS NOT NULL ", LocalTime.class, LocalTime.of( 23, 0, 0 ) );
        ass( "SELECT tsntz FROM t3 WHERE tsntz IS NULL ", LocalTime.class, null );
        ass( "SELECT t FROM t3 WHERE t IS NOT NULL ", LocalTime.class, LocalTime.of( 9, 50, 0 ) );
        ass( "SELECT t FROM t3 WHERE t IS NULL ", LocalTime.class, null );

        //
        // Time
        //
        ass( "SELECT tsntz FROM t3 WHERE tsntz IS NOT NULL ", Time.class, Time.valueOf( "23:00:00" ) );
        ass( "SELECT tsntz FROM t3 WHERE tsntz IS NULL ", Time.class, null );
        ass( "SELECT t FROM t3 WHERE t IS NOT NULL ", Time.class, Time.valueOf( "09:50:00" ) );
        ass( "SELECT t FROM t3 WHERE t IS NULL ", Time.class, null );

        //
        // java.util.Date
        //
        this.jdbcTemplate.update( "CREATE TABLE t4 ( tsntz TIMESTAMP WITHOUT TIME ZONE, d DATE )" );
        this.jdbcTemplate.update( "INSERT INTO t4 (tsntz,d) VALUES ('2014-12-31 23:00:00', '2014-12-31'), (NULL, NULL)" );
        assertEquals( ZonedDateTime.of( 2014, 12, 31, 23, 0, 0, 0, systemZone ).toInstant(),
                      this.jdbcTemplate.queryForObject( "SELECT tsntz FROM t2 WHERE tsntz IS NOT NULL ",
                                                        new SimpleDataTypeSingleColumnRowParser<>( java.util.Date.class ) ).toInstant() );
        assertNull( this.jdbcTemplate.queryForObject( "SELECT tsntz FROM t2 WHERE tsntz IS NULL ",
                                                      new SimpleDataTypeSingleColumnRowParser<>( java.util.Date.class ) ) );
        assertEquals( ZonedDateTime.of( 2014, 12, 31, 0, 0, 0, 0, systemZone ).toInstant(),
                      this.jdbcTemplate.queryForObject( "SELECT d FROM t2 WHERE d IS NOT NULL ",
                                                        new SimpleDataTypeSingleColumnRowParser<>( java.util.Date.class ) ).toInstant() );
        assertNull( this.jdbcTemplate.queryForObject( "SELECT d FROM t2 WHERE d IS NULL ",
                                                      new SimpleDataTypeSingleColumnRowParser<>( java.util.Date.class ) ) );
    }

    @Test
    public void enums() throws SQLException
    {
        this.jdbcTemplate.update( "CREATE TABLE t ( e VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t (e) VALUES ('JOE'), ('BLACK'), ('AGAIN'), (NULL)" );
        ass( "SELECT e FROM t WHERE e = 'JOE' ", TestEnum.class, TestEnum.JOE );
        ass( "SELECT e FROM t WHERE e = 'BLACK' ", TestEnum.class, TestEnum.BLACK );
        ass( "SELECT e FROM t WHERE e IS NULL ", TestEnum.class, null );
    }

    @Test( expected = EnumConstantNotPresentException.class )
    public void enumNotFound() throws SQLException
    {
        this.jdbcTemplate.update( "CREATE TABLE t ( e VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t (e) VALUES ('JOE'), ('BLACK'), ('AGAIN'), (NULL)" );
        ass( "SELECT e FROM t WHERE e = 'AGAIN' ", TestEnum.class, null );
    }

    @Test
    public void files() throws SQLException
    {
        this.jdbcTemplate.update( "CREATE TABLE t ( f VARCHAR(2048) )" );
        this.jdbcTemplate.update( "INSERT INTO t (f) VALUES ('/home/joe/black'), (NULL)" );
        ass( "SELECT f FROM t WHERE f IS NOT NULL ", File.class, new File( "/home/joe/black" ) );
        ass( "SELECT f FROM t WHERE f IS NULL ", File.class, null );
        ass( "SELECT f FROM t WHERE f IS NOT NULL ", Path.class, Paths.get( "/home/joe/black" ) );
        ass( "SELECT f FROM t WHERE f IS NULL ", Path.class, null );
    }

    @Test
    public void locales() throws SQLException
    {
        this.jdbcTemplate.update( "CREATE TABLE t ( l VARCHAR(255) )" );
        this.jdbcTemplate.update( "INSERT INTO t (l) VALUES ('en'), ('en-US'), ('il'), ('il-US'), (NULL)" );
        ass( "SELECT l FROM t WHERE l = 'en' ", Locale.class, new Locale( "en" ) );
        ass( "SELECT l FROM t WHERE l = 'en-US' ", Locale.class, new Locale( "en", "US" ) );
        ass( "SELECT l FROM t WHERE l = 'il' ", Locale.class, new Locale( "il" ) );
        ass( "SELECT l FROM t WHERE l = 'il-US' ", Locale.class, new Locale( "il", "US" ) );
        ass( "SELECT l FROM t WHERE l IS NULL ", Locale.class, null );
    }

    private <T> void ass( @Nonnull String sql, @Nonnull Class<T> type, @Nullable T expected ) throws SQLException
    {
        RowMapper<T> rowMapper = new SimpleDataTypeSingleColumnRowParser<>( type );
        if( expected == null )
        {
            assertNull( this.jdbcTemplate.queryForObject( sql, rowMapper ) );
        }
        else
        {
            assertEquals( expected, this.jdbcTemplate.queryForObject( sql, rowMapper ) );
        }
    }
}
