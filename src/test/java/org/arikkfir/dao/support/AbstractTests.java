package org.arikkfir.dao.support;

import java.util.UUID;
import org.junit.After;
import org.junit.Before;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

/**
 * @author arik
 * @on 1/4/15.
 */
public abstract class AbstractTests
{
    protected SingleConnectionDataSource dataSource;

    protected JdbcTemplate jdbcTemplate;

    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Before
    public void setUp() throws Exception
    {
        this.dataSource = new SingleConnectionDataSource( "jdbc:hsqldb:mem:" + UUID.randomUUID().toString(), "SA", "", true );
        this.jdbcTemplate = new JdbcTemplate( this.dataSource );
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate( this.dataSource );
    }

    @After
    public void tearDown() throws Exception
    {
        this.namedParameterJdbcTemplate = null;
        this.jdbcTemplate = null;
        this.dataSource.destroy();
        this.dataSource = null;
    }
}
