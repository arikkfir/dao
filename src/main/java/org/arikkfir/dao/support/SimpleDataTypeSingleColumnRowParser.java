package org.arikkfir.dao.support;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.*;
import java.util.Locale;
import java.util.UUID;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springframework.core.convert.ConversionService;
import org.springframework.jdbc.IncorrectResultSetColumnCountException;
import org.springframework.jdbc.core.RowMapper;

/**
 * @author arik
 */
public class SimpleDataTypeSingleColumnRowParser<T> implements RowMapper<T>
{
    @Nonnull
    private final Class<T> type;

    @Nullable
    private final ConversionService conversionService;

    public SimpleDataTypeSingleColumnRowParser( @Nonnull Class<T> type )
    {
        this( type, null );
    }

    public SimpleDataTypeSingleColumnRowParser( @Nonnull Class<T> type, @Nullable ConversionService conversionService )
    {
        this.type = type;
        this.conversionService = conversionService;
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public T mapRow( ResultSet rs, int rowNum ) throws SQLException
    {
        int columnCount = rs.getMetaData().getColumnCount();
        if( columnCount != 1 )
        {
            throw new IncorrectResultSetColumnCountException( "too many columns", 1, columnCount );
        }
        else if( this.type.equals( BigDecimal.class ) )
        {
            return ( T ) rs.getBigDecimal( 1 );
        }
        else if( this.type.equals( BigInteger.class ) )
        {
            String value = rs.getString( 1 );
            return value == null ? null : ( T ) new BigInteger( value );
        }
        else if( this.type.equals( boolean.class ) )
        {
            return ( T ) Boolean.valueOf( rs.getBoolean( 1 ) );
        }
        else if( this.type.equals( Boolean.class ) )
        {
            boolean value = rs.getBoolean( 1 );
            return rs.wasNull() ? null : this.type.cast( value );
        }
        else if( this.type.equals( byte.class ) )
        {
            return ( T ) Byte.valueOf( rs.getByte( 1 ) );
        }
        else if( this.type.equals( Byte.class ) )
        {
            byte value = rs.getByte( 1 );
            return rs.wasNull() ? null : this.type.cast( value );
        }
        else if( this.type.equals( byte[].class ) )
        {
            byte[] value = rs.getBytes( 1 );
            return rs.wasNull() ? null : this.type.cast( value );
        }
        else if( this.type.equals( char.class ) )
        {
            String value = rs.getString( 1 );
            return rs.wasNull() || value.isEmpty() ? ( T ) new Character( ( char ) 0 ) : ( T ) Character.valueOf( value.charAt( 0 ) );
        }
        else if( this.type.equals( Character.class ) )
        {
            String value = rs.getString( 1 );
            return rs.wasNull() || value.isEmpty() ? null : this.type.cast( value.charAt( 0 ) );
        }
        else if( this.type.equals( java.sql.Date.class ) )
        {
            return this.type.cast( rs.getDate( 1 ) );
        }
        else if( this.type.equals( java.util.Date.class ) )
        {
            Timestamp value = rs.getTimestamp( 1 );
            return rs.wasNull() ? null : ( T ) new java.util.Date( value.toInstant().toEpochMilli() );
        }
        else if( this.type.equals( double.class ) )
        {
            return ( T ) Double.valueOf( rs.getDouble( 1 ) );
        }
        else if( this.type.equals( Double.class ) )
        {
            double value = rs.getDouble( 1 );
            return rs.wasNull() ? null : ( T ) Double.valueOf( value );
        }
        else if( this.type.isEnum() )
        {
            String value = rs.getString( 1 );
            if( rs.wasNull() )
            {
                return null;
            }

            Class<? extends Enum> enumType = this.type.asSubclass( Enum.class );
            for( Enum enumConst : enumType.getEnumConstants() )
            {
                if( enumConst.name().equals( value ) )
                {
                    return this.type.cast( enumConst );
                }
            }

            throw new EnumConstantNotPresentException( enumType, value );
        }
        else if( this.type.equals( File.class ) )
        {
            String value = rs.getString( 1 );
            return rs.wasNull() ? null : this.type.cast( new File( value ) );
        }
        else if( this.type.equals( float.class ) )
        {
            return ( T ) Float.valueOf( rs.getFloat( 1 ) );
        }
        else if( this.type.equals( Float.class ) )
        {
            float value = rs.getFloat( 1 );
            return rs.wasNull() ? null : this.type.cast( value );
        }
        else if( this.type.equals( Instant.class ) )
        {
            Timestamp value = rs.getTimestamp( 1 );
            return value == null ? null : this.type.cast( value.toInstant() );
        }
        else if( this.type.equals( int.class ) )
        {
            return ( T ) Integer.valueOf( rs.getInt( 1 ) );
        }
        else if( this.type.equals( Integer.class ) )
        {
            int value = rs.getInt( 1 );
            return rs.wasNull() ? null : ( T ) Integer.valueOf( value );
        }
        else if( this.type.equals( LocalDate.class ) )
        {
            java.sql.Date value = rs.getDate( 1 );
            return value == null ? null : this.type.cast( value.toLocalDate() );
        }
        else if( this.type.equals( LocalDateTime.class ) )
        {
            Timestamp value = rs.getTimestamp( 1 );
            return value == null ? null : this.type.cast( value.toLocalDateTime() );
        }
        else if( this.type.equals( Locale.class ) )
        {
            String value = rs.getString( 1 );
            return value == null ? null : this.type.cast( Locale.forLanguageTag( value ) );
        }
        else if( this.type.equals( LocalTime.class ) )
        {
            Time value = rs.getTime( 1 );
            return value == null ? null : this.type.cast( value.toLocalTime() );
        }
        else if( this.type.equals( long.class ) )
        {
            return ( T ) Long.valueOf( rs.getLong( 1 ) );
        }
        else if( this.type.equals( Long.class ) )
        {
            long value = rs.getLong( 1 );
            return rs.wasNull() ? null : ( T ) Long.valueOf( value );
        }
        else if( this.type.equals( Object.class ) )
        {
            return ( T ) rs.getObject( 1 );
        }
        else if( this.type.equals( Path.class ) )
        {
            String value = rs.getString( 1 );
            return rs.wasNull() ? null : this.type.cast( Paths.get( value ) );
        }
        else if( this.type.equals( short.class ) )
        {
            return ( T ) Short.valueOf( rs.getShort( 1 ) );
        }
        else if( this.type.equals( Short.class ) )
        {
            short value = rs.getShort( 1 );
            return rs.wasNull() ? null : this.type.cast( value );
        }
        else if( this.type.equals( String.class ) )
        {
            return this.type.cast( rs.getString( 1 ) );
        }
        else if( this.type.equals( Time.class ) )
        {
            return this.type.cast( rs.getTime( 1 ) );
        }
        else if( this.type.equals( Timestamp.class ) )
        {
            return this.type.cast( rs.getTimestamp( 1 ) );
        }
        else if( this.type.equals( URI.class ) )
        {
            String value = rs.getString( 1 );
            return value == null ? null : this.type.cast( URI.create( value ) );
        }
        else if( this.type.equals( URL.class ) )
        {
            String value = rs.getString( 1 );
            try
            {
                return value == null ? null : this.type.cast( new URL( value ) );
            }
            catch( MalformedURLException e )
            {
                throw new IllegalStateException( "could not create a URL from '" + value + "'" );
            }
        }
        else if( this.type.equals( UUID.class ) )
        {
            String value = rs.getString( 1 );
            return value == null ? null : this.type.cast( UUID.fromString( value ) );
        }
        else if( this.type.equals( ZonedDateTime.class ) )
        {
            Timestamp value = rs.getTimestamp( 1 );
            return value == null ? null : this.type.cast( ZonedDateTime.ofInstant( value.toInstant(), ZoneId.systemDefault() ) );
        }
        else if( this.conversionService != null )
        {
            return this.conversionService.convert( rs.getObject( 1 ), this.type );
        }
        else
        {
            throw new IllegalStateException( "unsupported type for JDBC mapping: " + this.type.getName() );
        }
    }
}
