package org.arikkfir.dao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

/**
 * @author arik
 */
@Retention( RetentionPolicy.RUNTIME )
@Target( ElementType.METHOD )
public @interface Query
{
    String value();

    Class<? extends RowMapper> rowMapper() default RowMapper.class;

    Class<? extends ResultSetExtractor> resultSetExtractor() default ResultSetExtractor.class;
}
