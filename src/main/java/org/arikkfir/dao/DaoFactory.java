package org.arikkfir.dao;

import javax.annotation.Nonnull;

/**
 * @author arik
 */
public interface DaoFactory
{
    @Nonnull
    <T> T create( @Nonnull Class<T> daoType );
}
