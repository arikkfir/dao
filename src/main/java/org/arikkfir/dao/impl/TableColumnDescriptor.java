package org.arikkfir.dao.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.annotation.Nonnull;
import org.arikkfir.dao.Column;
import org.arikkfir.dao.Id;
import org.springframework.core.annotation.AnnotationUtils;

import static java.lang.String.format;
import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;

/**
 * @author arik
 */
class TableColumnDescriptor
{
    @Nonnull
    private final String columnName;

    @Nonnull
    private final Set<Method> getters = new LinkedHashSet<>();

    @Nonnull
    private final Set<Method> setters = new LinkedHashSet<>();

    private boolean idColumn;

    public TableColumnDescriptor( @Nonnull String columnName )
    {
        this.columnName = columnName.toLowerCase();
    }

    @Nonnull
    public String getColumnName()
    {
        return this.columnName;
    }

    public boolean isIdColumn()
    {
        return this.idColumn;
    }

    public <T> T getValue( @Nonnull Class<T> type, @Nonnull Object instance ) throws InvocationTargetException, IllegalAccessException
    {
        for( Method getter : this.getters )
        {
            if( getter.getReturnType().equals( type ) )
            {
                return type.cast( getter.invoke( instance ) );
            }
        }

        for( Method getter : this.getters )
        {
            if( type.isAssignableFrom( getter.getReturnType() ) )
            {
                return type.cast( getter.invoke( instance ) );
            }
        }

        throw new IllegalArgumentException( "no getter for @Column '" + this.columnName + "' found with type '" + type.getName() + "'" );
    }

    void addGetter( @Nonnull Method method )
    {
        Column columnAnn = AnnotationUtils.findAnnotation( method, Column.class );
        if( columnAnn == null )
        {
            throw new IllegalArgumentException( "method '" + method.toGenericString() + "' does not have @Column annotation" );
        }

        if( !this.columnName.equals( columnAnn.value().toLowerCase() ) )
        {
            String msg = format( "method '%s' is mapped to column '%s' and not to '%s'", method.toGenericString(), columnAnn.value(), this.columnName );
            throw new IllegalArgumentException( msg );
        }

        Id idAnn = findAnnotation( method, Id.class );
        if( idAnn != null )
        {
            if( !this.setters.isEmpty() )
            {
                String msg = "method '" + method.toGenericString() + "' is annotated by @Id, but its column ('" + this.columnName + "') is an @Id column";
                throw new IllegalArgumentException( msg );
            }
            else if( !method.getReturnType().equals( int.class ) )
            {
                String msg = "method '" + method.toGenericString() + "' is annotated by @Id, must return @Id";
                throw new IllegalArgumentException( msg );
            }
            else
            {
                this.idColumn = true;
            }
        }

        this.getters.add( method );
    }

    void addSetter( @Nonnull Method method )
    {
        Column columnAnn = AnnotationUtils.findAnnotation( method, Column.class );
        if( columnAnn == null )
        {
            throw new IllegalArgumentException( "method '" + method.toGenericString() + "' does not have @Column annotation" );
        }

        if( !this.columnName.equals( columnAnn.value().toLowerCase() ) )
        {
            String msg = format( "method '%s' is mapped to column '%s' and not to '%s'", method.toGenericString(), columnAnn.value(), this.columnName );
            throw new IllegalArgumentException( msg );
        }

        if( this.idColumn )
        {
            String msg = "method '" + method.toGenericString() + "' is mapped to column '" + columnAnn
                    .value() + "', but that column is an @Id column, thus cannot have setters";
            throw new IllegalArgumentException( msg );
        }

        if( findAnnotation( method, Id.class ) != null )
        {
            String msg = "method '" + method.toGenericString() + "' is annotated with @Id - but @Id columns cannot have setters";
            throw new IllegalArgumentException( msg );
        }

        this.setters.add( method );
    }
}
