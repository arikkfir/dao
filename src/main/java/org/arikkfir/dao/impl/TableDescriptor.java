package org.arikkfir.dao.impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.arikkfir.dao.Column;
import org.arikkfir.dao.Delete;
import org.arikkfir.dao.Table;
import org.arikkfir.dao.support.SimpleDataTypeSingleColumnRowParser;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import static java.lang.String.format;
import static java.lang.reflect.Proxy.newProxyInstance;
import static java.util.Collections.singletonMap;
import static java.util.Collections.unmodifiableMap;
import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;
import static org.springframework.util.ReflectionUtils.getUniqueDeclaredMethods;

/**
 * @author arik
 */
class TableDescriptor
{
    private static final Object[] EMPTY_ARGS_ARRAY = new Object[ 0 ];

    @Nonnull
    private final DaoFactoryImpl daoFactory;

    @Nonnull
    private final Class<?> type;

    @Nonnull
    private final Class<?>[] typeAsArray;

    @Nonnull
    private final String tableName;

    @Nonnull
    private final TableColumnDescriptor idColumn;

    @Nonnull
    private final Map<Method, TableColumnDescriptor> columnsForGetters;

    @Nonnull
    private final Map<Method, TableColumnDescriptor> columnsForSetters;

    public TableDescriptor( @Nonnull DaoFactoryImpl daoFactory, @Nonnull Class<?> type )
    {
        this.daoFactory = daoFactory;
        this.type = type;
        this.typeAsArray = new Class<?>[] { this.type };

        Table tableAnn = findAnnotation( this.type, Table.class );
        if( tableAnn == null )
        {
            String msg = format( "@Table not found on '%s'", this.type.getName() );
            throw new IllegalArgumentException( msg );
        }
        else
        {
            this.tableName = tableAnn.value();
        }

        Map<String, TableColumnDescriptor> columns = new HashMap<>();
        Map<Method, TableColumnDescriptor> columnsForGetters = new HashMap<>();
        Map<Method, TableColumnDescriptor> columnsForSetters = new HashMap<>();
        for( Method method : getUniqueDeclaredMethods( this.type ) )
        {
            Column columnAnn = findAnnotation( method, Column.class );
            if( columnAnn != null )
            {
                String columnName = columnAnn.value();
                TableColumnDescriptor tableColumnDescriptor = columns.get( columnName );
                if( tableColumnDescriptor == null )
                {
                    tableColumnDescriptor = new TableColumnDescriptor( columnName );
                }

                if( isColumnSetter( method ) )
                {
                    tableColumnDescriptor.addSetter( method );
                    columnsForSetters.put( method, tableColumnDescriptor );
                }
                else if( isColumnGetter( method ) )
                {
                    tableColumnDescriptor.addGetter( method );
                    columnsForGetters.put( method, tableColumnDescriptor );
                }
                else
                {
                    throw new IllegalArgumentException( "methods in @Table classes must be either @Column getters or setters" );
                }
                columns.put( columnName, tableColumnDescriptor );
            }
        }

        this.columnsForGetters = unmodifiableMap( columnsForGetters );
        this.columnsForSetters = unmodifiableMap( columnsForSetters );

        List<TableColumnDescriptor> idColumns = columns.values()
                                                       .stream()
                                                       .filter( TableColumnDescriptor::isIdColumn )
                                                       .collect( Collectors.toList() );
        if( idColumns.isEmpty() )
        {
            throw new IllegalArgumentException( "no @Id methods found on '" + this.type.getName() + "'" );
        }
        else if( idColumns.size() > 1 )
        {
            throw new IllegalArgumentException( "too many @Id methods found on '" + this.type.getName() + "'" );
        }
        else
        {
            this.idColumn = idColumns.get( 0 );
        }
    }

    @Nonnull
    Class<?> getType()
    {
        return this.type;
    }

    @Nonnull
    String getTableName()
    {
        return this.tableName;
    }

    @Nonnull
    String getIdColumnName()
    {
        return this.idColumn.getColumnName();
    }

    @Nonnull
    Object createFrom( int id )
    {
        return newProxyInstance( this.type.getClassLoader(), this.typeAsArray, new TableRowInvocationHandler( id ) );
    }

    private boolean isColumnGetter( Method method )
    {
        return !void.class.equals( method.getReturnType() ) && method.getParameters().length == 0;
    }

    private boolean isColumnSetter( Method method )
    {
        return void.class.equals( method.getReturnType() ) && method.getParameters().length == 1;
    }

    private class TableRowInvocationHandler implements InvocationHandler
    {
        private final int id;

        private TableRowInvocationHandler( int id )
        {
            this.id = id;
        }

        @Override
        public Object invoke( Object proxy, Method method, Object[] args ) throws Throwable
        {
            args = args == null ? EMPTY_ARGS_ARRAY : args;

            if( isEqualsMethod( method ) )
            {
                //
                // this is the 'equals' method
                //
                Object that = args[ 0 ];
                TableDescriptor thatTableDesc = TableDescriptor.this.daoFactory.getTableDescriptor( that.getClass() );
                if( TableDescriptor.this.type.equals( thatTableDesc.type ) )
                {
                    int thatId = TableDescriptor.this.idColumn.getValue( int.class, that );
                    return this.id == thatId;
                }
                return false;
            }
            else if( isHashCodeMethod( method ) )
            {
                //
                // this is the 'hashCode' method
                //
                return this.id;
            }
            else if( isToStringMethod( method ) )
            {
                //
                // this is the 'toString' method
                //
                return TableDescriptor.this.type.getSimpleName() + "[" + this.id + "]";
            }

            TableColumnDescriptor tableColumnDescriptor;

            tableColumnDescriptor = TableDescriptor.this.columnsForGetters.get( method );
            if( tableColumnDescriptor != null )
            {
                //
                // a @Column getter method
                //
                return fetchColumnValue( tableColumnDescriptor, method.getReturnType() );
            }

            tableColumnDescriptor = TableDescriptor.this.columnsForSetters.get( method );
            if( tableColumnDescriptor != null )
            {
                //
                // a @Column setter method
                //
                updateColumnValue( tableColumnDescriptor, args[ 0 ] );
                return null;
            }

            if( void.class.equals( method.getReturnType() ) && findAnnotation( method, Delete.class ) != null )
            {
                //
                // a @Delete method
                //
                deleteRow();
                return null;
            }

            String msg = format( "method '%s.%s' is not supported", TableDescriptor.this.type.getSimpleName(), method.getName() );
            throw new UnsupportedOperationException( msg );
        }

        private boolean isToStringMethod( Method method )
        {
            return "toString".equals( method.getName() ) && method.getParameters().length == 0;
        }

        private boolean isHashCodeMethod( Method method )
        {
            return "hashCode".equals( method.getName() ) && method.getParameters().length == 0;
        }

        private boolean isEqualsMethod( Method method )
        {
            return "equals".equals( method.getName() ) && method.getParameters().length == 1 && Object.class.equals( method.getParameters()[ 0 ].getType() );
        }

        @Nullable
        private Object fetchColumnValue( @Nonnull TableColumnDescriptor tableColumnDescriptor, @Nonnull Class<?> type )
                throws SQLException
        {
            NamedParameterJdbcTemplate namedJdbcTemplate = TableDescriptor.this.daoFactory.getNamedJdbcTemplate();
            String sql = format( "SELECT %s FROM %s WHERE %s = :id",
                                 tableColumnDescriptor.getColumnName(),
                                 TableDescriptor.this.tableName,
                                 TableDescriptor.this.idColumn.getColumnName() );

            Table tableAnn = findAnnotation( type, Table.class );
            if( tableAnn != null )
            {
                Integer id = namedJdbcTemplate.queryForObject(
                        sql,
                        singletonMap( "id", this.id ),
                        Integer.class );
                if( id == null )
                {
                    return null;
                }
                else
                {
                    TableDescriptor tableDescriptor = TableDescriptor.this.daoFactory.getTableDescriptor( type );
                    return tableDescriptor.createFrom( id );
                }
            }
            else
            {
                return namedJdbcTemplate.queryForObject(
                        sql,
                        singletonMap( "id", this.id ),
                        new SimpleDataTypeSingleColumnRowParser<>( type ) );
            }
        }

        private int updateColumnValue( @Nonnull TableColumnDescriptor tableColumnDescriptor, @Nullable Object value )
                throws SQLException
        {
            String sql = format( "UPDATE %s SET %s = :value WHERE %s = :id",
                                 TableDescriptor.this.tableName,
                                 tableColumnDescriptor.getColumnName(),
                                 TableDescriptor.this.idColumn.getColumnName() );
            Map<String, Object> updateArgs = new HashMap<>();
            updateArgs.put( "value", TableDescriptor.this.daoFactory.convertToJdbcType( value ) );
            updateArgs.put( "id", this.id );
            return TableDescriptor.this.daoFactory.getNamedJdbcTemplate().update( sql, updateArgs );
        }

        private int deleteRow() throws SQLException
        {
            String sql = format( "DELETE FROM %s WHERE %s = :id",
                                 TableDescriptor.this.tableName,
                                 TableDescriptor.this.idColumn.getColumnName() );
            return TableDescriptor.this.daoFactory.getNamedJdbcTemplate().update( sql, singletonMap( "id", this.id ) );
        }
    }
}
