package org.arikkfir.dao.impl;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.time.*;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.arikkfir.dao.DaoFactory;
import org.arikkfir.dao.Table;
import org.slf4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 */
public class DaoFactoryImpl implements DaoFactory
{
    private static final Logger LOG = getLogger( DaoFactoryImpl.class );

    @Nonnull
    private final JdbcTemplate jdbcTemplate;

    @Nonnull
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    @Nonnull
    private final Map<Class<?>, DaoDescriptor> daoDescriptors = new ConcurrentHashMap<>();

    @Nonnull
    private final Map<Class<?>, TableDescriptor> tableDescriptors = new ConcurrentHashMap<>();

    public DaoFactoryImpl( @Nonnull JdbcTemplate jdbcTemplate, @Nonnull NamedParameterJdbcTemplate namedJdbcTemplate )
    {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    @Nonnull
    @Override
    public <T> T create( @Nonnull Class<T> type )
    {
        LOG.debug( "Creating a DAO of type '{}'", type.getName() );
        DaoDescriptor daoDescriptor = this.daoDescriptors.computeIfAbsent( type, t -> new DaoDescriptor( this, t ) );
        return type.cast( daoDescriptor.getInstance() );
    }

    public void destroy()
    {
        this.tableDescriptors.clear();
        this.daoDescriptors.clear();
    }

    @Nonnull
    TableDescriptor getTableDescriptor( @Nonnull Class<?> type )
    {
        return this.tableDescriptors.computeIfAbsent( type, t -> {
            Table tableAnn = AnnotationUtils.findAnnotation( t, Table.class );
            if( tableAnn != null )
            {
                return new TableDescriptor( this, t );
            }
            else
            {
                throw new IllegalArgumentException( "type '" + t.getName() + "' has no @Table annotation" );
            }
        } );

    }

    @Nonnull
    JdbcTemplate getJdbcTemplate()
    {
        return this.jdbcTemplate;
    }

    @Nonnull
    NamedParameterJdbcTemplate getNamedJdbcTemplate()
    {
        return this.namedJdbcTemplate;
    }

    @Nullable
    Object convertToJdbcType( @Nullable Object value )
    {
        if( value == null )
        {
            return null;
        }

        Class<?> type = value.getClass();
        if( type.isEnum() )
        {
            return ( ( Enum ) value ).name();
        }
        else if( type.equals( File.class ) )
        {
            return ( ( File ) value ).getPath();
        }
        else if( type.equals( Instant.class ) )
        {
            return new Timestamp( ( ( Instant ) value ).toEpochMilli() );
        }
        else if( type.equals( LocalDate.class ) )
        {
            LocalDate localDate = ( LocalDate ) value;
            int year = localDate.getYear();
            Month month = localDate.getMonth();
            int dayOfMonth = localDate.getDayOfMonth();
            long timeInMillis = new GregorianCalendar( year, month.getValue() - 1, dayOfMonth ).getTimeInMillis();
            return new java.sql.Date( timeInMillis );
        }
        else if( type.equals( LocalDateTime.class ) )
        {
            LocalDateTime localDate = ( LocalDateTime ) value;
            int year = localDate.getYear();
            Month month = localDate.getMonth();
            int dayOfMonth = localDate.getDayOfMonth();
            int hour = localDate.getHour();
            int minute = localDate.getMinute();
            int second = localDate.getSecond();
            long timeInMillis = new GregorianCalendar( year, month.getValue() - 1, dayOfMonth, hour, minute, second ).getTimeInMillis();
            return new java.sql.Date( timeInMillis );
        }
        else if( type.equals( Locale.class ) )
        {
            return ( ( Locale ) value ).toLanguageTag();
        }
        else if( type.equals( LocalTime.class ) )
        {
            LocalTime localTime = ( LocalTime ) value;
            int hour = localTime.getHour();
            int minute = localTime.getMinute();
            int second = localTime.getSecond();
            long timeInMillis = new GregorianCalendar( 1970, 0, 1, hour, minute, second ).getTimeInMillis();
            return new java.sql.Time( timeInMillis );
        }
        else if( type.equals( Path.class ) )
        {
            return value.toString();
        }
        else if( type.equals( URI.class ) )
        {
            return value.toString();
        }
        else if( type.equals( URL.class ) )
        {
            return value.toString();
        }
        else if( type.equals( UUID.class ) )
        {
            return value.toString();
        }
        else if( type.equals( ZonedDateTime.class ) )
        {
            return new Timestamp( ( ( ZonedDateTime ) value ).toInstant().toEpochMilli() );
        }
        else
        {
            return value;
        }
    }
}
