package org.arikkfir.dao.impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Proxy;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import org.arikkfir.dao.Column;
import org.arikkfir.dao.Query;
import org.arikkfir.dao.Table;
import org.arikkfir.dao.Update;
import org.arikkfir.dao.support.SimpleDataTypeSingleColumnRowParser;
import org.springframework.core.ResolvableType;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import static java.lang.String.format;
import static java.lang.reflect.Proxy.getInvocationHandler;
import static java.lang.reflect.Proxy.newProxyInstance;
import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;
import static org.springframework.util.ReflectionUtils.findMethod;
import static org.springframework.util.StringUtils.capitalize;

/**
 * @author arik
 */
class DaoDescriptor
{
    private static final Object[] EMPTY_ARGS_ARRAY = new Object[ 0 ];

    @Nonnull
    private final DaoFactoryImpl daoFactory;

    @Nonnull
    private final Class<?> type;

    @Nonnull
    private final Object instance;

    public DaoDescriptor( @Nonnull DaoFactoryImpl daoFactory, @Nonnull Class<?> type )
    {
        this.daoFactory = daoFactory;
        this.type = type;
        this.instance = newProxyInstance( this.type.getClassLoader(), new Class<?>[] { this.type }, new DaoInvocationHandler() );
    }

    @Nonnull
    Object getInstance()
    {
        return this.instance;
    }

    private class DaoInvocationHandler implements InvocationHandler
    {
        @Override
        public Object invoke( Object proxy, Method method, Object[] args ) throws Throwable
        {
            args = args == null ? EMPTY_ARGS_ARRAY : args;

            if( isEqualsMethod( method ) )
            {
                Object that = args[ 0 ];
                if( that == null )
                {
                    return false;
                }

                if( !Proxy.isProxyClass( that.getClass() ) )
                {
                    return false;
                }

                InvocationHandler thatInvocationHandler = getInvocationHandler( that );
                if( !DaoInvocationHandler.class.isInstance( thatInvocationHandler ) )
                {
                    return false;
                }

                DaoInvocationHandler daoInvocationHandler = ( DaoInvocationHandler ) thatInvocationHandler;
                return Objects.equals( DaoDescriptor.this.type, daoInvocationHandler.getType() );
            }
            else if( isHashCodeMethod( method ) )
            {
                return hashCode();
            }
            else if( isToStringMethod( method ) )
            {
                return format( "Dao:%s[%d]", DaoDescriptor.this.type.getSimpleName(), hashCode() );
            }

            Query queryAnn = findAnnotation( method, Query.class );
            if( queryAnn != null )
            {
                return executeQuery( method, queryAnn, args );
            }

            Update updateAnn = findAnnotation( method, Update.class );
            if( updateAnn != null )
            {
                return executeUpdate( method, updateAnn, args );
            }

            Class<?> returnType = method.getReturnType();
            if( !void.class.equals( returnType ) )
            {
                Table tableAnn = findAnnotation( returnType, Table.class );
                if( tableAnn != null )
                {
                    if( method.getName().startsWith( "create" ) )
                    {
                        return executeInsert( method, args );
                    }
                }
            }

            throw new UnsupportedOperationException(
                    format( "method '%s' in DAO '%s' is not supported", method.toGenericString(), DaoDescriptor.this.type.getSimpleName() ) );
        }

        @Nonnull
        private Class<?> getType()
        {
            return DaoDescriptor.this.type;
        }

        private boolean isToStringMethod( Method method )
        {
            return "toString".equals( method.getName() ) && method.getParameters().length == 0;
        }

        private boolean isHashCodeMethod( Method method )
        {
            return "hashCode".equals( method.getName() ) && method.getParameters().length == 0;
        }

        private boolean isEqualsMethod( Method method )
        {
            return "equals".equals( method.getName() ) && method.getParameters().length == 1 && Object.class.equals( method.getParameters()[ 0 ].getType() );
        }

        @SuppressWarnings( "unchecked" )
        private Object executeQuery( @Nonnull Method method, Query queryAnn, @Nonnull Object[] args )
                throws SQLException
        {
            NamedParameterJdbcTemplate namedJdbcTemplate = DaoDescriptor.this.daoFactory.getNamedJdbcTemplate();

            Map<String, Object> queryArgs = new HashMap<>();

            ResolvableType resultSetExtractorResolvableType = null;
            ResultSetExtractor<?> resultSetExtractor = null;
            ResolvableType rowMapperResolvableType = null;
            RowMapper<?> rowMapper = null;

            Class<? extends ResultSetExtractor> resultSetExtractorType = queryAnn.resultSetExtractor();
            Class<? extends RowMapper> rowMapperType = queryAnn.rowMapper();
            if( !resultSetExtractorType.equals( ResultSetExtractor.class ) )
            {
                resultSetExtractorResolvableType = ResolvableType.forClass( resultSetExtractorType );
                try
                {
                    resultSetExtractor = resultSetExtractorType.newInstance();
                }
                catch( Exception e )
                {
                    throw new IllegalStateException( "could not create ResultSetExtractor of '" + resultSetExtractorType.getName() + "'", e );
                }
            }
            else if( !rowMapperType.equals( RowMapper.class ) )
            {
                rowMapperResolvableType = ResolvableType.forClass( rowMapperType );
                try
                {
                    rowMapper = rowMapperType.newInstance();
                }
                catch( Exception e )
                {
                    throw new IllegalStateException( "could not create RowMapper of '" + resultSetExtractorType.getName() + "'", e );
                }
            }

            Parameter[] parameters = method.getParameters();
            for( int i = 0; i < parameters.length; i++ )
            {
                Parameter parameter = parameters[ i ];
                Class<?> type = parameter.getType();
                if( type.isAssignableFrom( ResultSetExtractor.class ) )
                {
                    resultSetExtractorResolvableType = ResolvableType.forMethodParameter( method, i );
                    resultSetExtractor = ResultSetExtractor.class.cast( args[ i ] );
                }
                else if( type.isAssignableFrom( RowMapper.class ) )
                {
                    rowMapperResolvableType = ResolvableType.forMethodParameter( method, i );
                    rowMapper = RowMapper.class.cast( args[ i ] );
                }
                else
                {
                    queryArgs.put( parameter.getName(), DaoDescriptor.this.daoFactory.convertToJdbcType( args[ i ] ) );
                }
            }

            ResolvableType methodResolvableReturnType = ResolvableType.forMethodReturnType( method );
            if( resultSetExtractor != null && rowMapper != null )
            {
                String msg = format(
                        "method '%s.%s' has both ResultSetExtractor and RowMapper parameters - only one can be specified",
                        DaoDescriptor.this.type.getSimpleName(), method.getName() );
                throw new IllegalStateException( msg );
            }
            else if( resultSetExtractor != null )
            {
                if( !methodResolvableReturnType.isAssignableFrom( resultSetExtractorResolvableType.as( ResultSetExtractor.class ).getGeneric( 0 ) ) )
                {
                    String msg = format(
                            "result of ResultSetExtractor parameter of '%s.%s' is not compatible with method return type",
                            DaoDescriptor.this.type.getSimpleName(), method.getName() );
                    throw new IllegalStateException( msg );
                }
                else
                {
                    return namedJdbcTemplate.query( queryAnn.value(), queryArgs, resultSetExtractor );
                }
            }
            else if( rowMapper != null )
            {
                if( methodResolvableReturnType.getRawClass().equals( List.class ) )
                {
                    ResolvableType listItemResolvableType = methodResolvableReturnType.getGeneric( 0 );
                    ResolvableType rowMapperResultResolvableType = rowMapperResolvableType.as( RowMapper.class ).getGeneric( 0 );
                    if( !listItemResolvableType.isAssignableFrom( rowMapperResultResolvableType ) )
                    {
                        String msg = format(
                                "result of RowMapper parameter of '%s.%s' is not compatible with the method's List return type",
                                DaoDescriptor.this.type.getSimpleName(), method.getName() );
                        throw new IllegalStateException( msg );
                    }
                    else
                    {
                        return namedJdbcTemplate.query( queryAnn.value(), queryArgs, rowMapper );
                    }
                }
                else if( methodResolvableReturnType.isAssignableFrom( rowMapperResolvableType.as( RowMapper.class ).getGeneric( 0 ) ) )
                {
                    List<?> rows = namedJdbcTemplate.query( queryAnn.value(), queryArgs, rowMapper );
                    if( rows.isEmpty() )
                    {
                        if( method.isAnnotationPresent( Nonnull.class ) )
                        {
                            String msg = format( "method '%s.%s' is @Nonnull, but no rows returned for its query",
                                                 DaoDescriptor.this.type.getSimpleName(), method.getName() );
                            throw new IncorrectResultSizeDataAccessException( msg, 1, 0 );
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else if( rows.size() > 1 )
                    {
                        String msg = format( "method '%s.%s' expects a single result, but more than one row was found",
                                             DaoDescriptor.this.type.getSimpleName(), method.getName() );
                        throw new IncorrectResultSizeDataAccessException( msg, 1, rows.size() );
                    }
                    else
                    {
                        return rows.get( 0 );
                    }
                }
                else
                {
                    String msg = format(
                            "result of RowMapper parameter of '%s.%s' is not compatible with method return type",
                            DaoDescriptor.this.type.getSimpleName(), method.getName() );
                    throw new IllegalStateException( msg );
                }
            }
            else if( methodResolvableReturnType.getRawClass().equals( List.class ) )
            {
                Class<?> listItemType = methodResolvableReturnType.resolveGeneric( 0 );
                Table listItemTypeTableAnn = findAnnotation( listItemType, Table.class );
                if( listItemTypeTableAnn != null )
                {
                    return namedJdbcTemplate.query( queryAnn.value(), queryArgs, ( rs, rowNum ) -> {
                        TableDescriptor tableDescriptor = DaoDescriptor.this.daoFactory.getTableDescriptor( listItemType );
                        return listItemType.cast( tableDescriptor.createFrom( rs.getInt( 1 ) ) );
                    } );
                }
                else
                {
                    return namedJdbcTemplate.query( queryAnn.value(), queryArgs, new SimpleDataTypeSingleColumnRowParser( listItemType ) );
                }
            }
            else
            {
                Class<?> type = methodResolvableReturnType.getRawClass();
                Table returnTypeTableAnn = findAnnotation( type, Table.class );
                if( returnTypeTableAnn != null )
                {
                    List<Object> result = namedJdbcTemplate.query( queryAnn.value(), queryArgs, ( rs, rowNum ) -> {
                        TableDescriptor tableDescriptor = DaoDescriptor.this.daoFactory.getTableDescriptor( type );
                        return type.cast( tableDescriptor.createFrom( rs.getInt( 1 ) ) );
                    } );

                    if( result.isEmpty() )
                    {
                        if( method.isAnnotationPresent( Nonnull.class ) )
                        {
                            String msg = format( "method '%s.%s' is @Nonnull, but no rows returned for its query",
                                                 DaoDescriptor.this.type.getSimpleName(), method.getName() );
                            throw new IncorrectResultSizeDataAccessException( msg, 1, 0 );
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return result.get( 0 );
                    }
                }
                else
                {
                    List<Object> result = namedJdbcTemplate.query( queryAnn.value(), queryArgs, new SimpleDataTypeSingleColumnRowParser( type ) );
                    if( result.isEmpty() )
                    {
                        if( method.isAnnotationPresent( Nonnull.class ) )
                        {
                            String msg = format( "method '%s.%s' is @Nonnull, but no rows returned for its query",
                                                 DaoDescriptor.this.type.getSimpleName(), method.getName() );
                            throw new IncorrectResultSizeDataAccessException( msg, 1, 0 );
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return result.get( 0 );
                    }
                }
            }
        }

        private Object executeUpdate( @Nonnull Method method, Update updateAnn, @Nonnull Object[] args )
        {
            Class<?> returnType = method.getReturnType();
            if( !int.class.equals( returnType ) && !void.class.equals( returnType ) )
            {
                String msg = format( "method '%s.%s' is annotated with @Update, but returns neither 'int' nor 'void'",
                                     DaoDescriptor.this.type.getSimpleName(), method.getName() );
                throw new IllegalStateException( msg );
            }

            Map<String, Object> queryArgs = new HashMap<>();
            Parameter[] parameters = method.getParameters();
            for( int i = 0; i < parameters.length; i++ )
            {
                queryArgs.put( parameters[ i ].getName(), DaoDescriptor.this.daoFactory.convertToJdbcType( args[ i ] ) );
            }
            return DaoDescriptor.this.daoFactory.getNamedJdbcTemplate().update( updateAnn.value(), queryArgs );
        }

        private Object executeInsert( @Nonnull Method method, @Nonnull Object[] args ) throws SQLException
        {
            TableDescriptor tableDescriptor = DaoDescriptor.this.daoFactory.getTableDescriptor( method.getReturnType() );

            Map<String, Object> queryArgs = new HashMap<>();
            Parameter[] parameters = method.getParameters();
            for( int i = 0; i < parameters.length; i++ )
            {
                Parameter parameter = parameters[ i ];

                String columnName;
                Column columnAnn = parameter.getAnnotation( Column.class );
                if( columnAnn != null )
                {
                    columnName = columnAnn.value();
                }
                else
                {
                    Method getter = null;
                    if( parameter.getType().equals( Boolean.class ) || parameter.getType().equals( boolean.class ) )
                    {
                        getter = findMethod( tableDescriptor.getType(), "is" + capitalize( parameter.getName() ) );
                    }
                    if( getter == null )
                    {
                        getter = findMethod( tableDescriptor.getType(), "get" + capitalize( parameter.getName() ) );
                    }

                    if( getter != null )
                    {
                        columnAnn = findAnnotation( getter, Column.class );
                        if( columnAnn != null )
                        {
                            columnName = columnAnn.value();
                        }
                        else
                        {
                            String msg = format( "could not infer column name for parameter '%s' of method '%s.%s'",
                                                 parameter.getName(), DaoDescriptor.this.type.getName(), method.getName() );
                            throw new IllegalArgumentException( msg );
                        }
                    }
                    else
                    {
                        String msg = format( "could not infer column name for parameter '%s' of method '%s.%s'",
                                             parameter.getName(), DaoDescriptor.this.type.getName(), method.getName() );
                        throw new IllegalArgumentException( msg );
                    }
                }
                queryArgs.put( columnName, DaoDescriptor.this.daoFactory.convertToJdbcType( args[ i ] ) );
            }

            SimpleJdbcInsert insert = new SimpleJdbcInsert( DaoDescriptor.this.daoFactory.getJdbcTemplate() );
            insert.setColumnNames( queryArgs.keySet().stream().collect( Collectors.toList() ) );
            insert.setGeneratedKeyName( tableDescriptor.getIdColumnName() );
            insert.setTableName( tableDescriptor.getTableName() );
            Number id = insert.executeAndReturnKey( queryArgs );
            return tableDescriptor.createFrom( id.intValue() );
        }
    }
}
